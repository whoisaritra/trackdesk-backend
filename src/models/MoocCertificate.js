const { Model, DataTypes } = require('sequelize');
const sequelize = require('../database')

class MoocCertificate extends Model { }
MoocCertificate.init({
    id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    image: {
      type: DataTypes.STRING,
      allowNull: true,
      unique: true,
    },
    duration: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    date: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    year: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    courseName: {
      type: DataTypes.STRING,
      allowNull: false
    },
}, { sequelize, modelName: 'mooccertificates', timestamps: true });

module.exports = MoocCertificate
