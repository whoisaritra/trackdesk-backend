const { Model, DataTypes } = require('sequelize');
const sequelize = require('../database')

class MarCertificate extends Model { }
MarCertificate.init({
    id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    image: {
      type: DataTypes.STRING,
      allowNull: true,
      unique: true,
    },
    date: {
      type: DataTypes.STRING,
      allowNull: true,
    },
    year: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    eventName: {
      type: DataTypes.STRING,
      allowNull: false
    },
}, { sequelize, modelName: 'marcertificates', timestamps: true });

module.exports = MarCertificate
