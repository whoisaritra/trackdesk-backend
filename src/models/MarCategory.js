const { Model, DataTypes } = require('sequelize');
const sequelize = require('../database')

class MarCategory extends Model { }
MarCategory .init({
    id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    category: {
      type: DataTypes.STRING,
      allowNull: true,
      unique: true,
    },
    points: {
      type: DataTypes.STRING,
      allowNull: true,
    },
    max_points: {
      type: DataTypes.STRING,
      allowNull: false
    },

}, { sequelize, modelName: 'marcategories', timestamps: true });

module.exports = MarCategory
