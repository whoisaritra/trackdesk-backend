const Student = require('./Student')
const Department = require('./Department')
const Faculty = require('./Faculty')
const Admin = require('./Admin')

const MarCategory = require('./MarCategory')
const MarCertificate = require('./MarCertificate')
const MoocCertificate = require('./MoocCertificate')


MarCategory.hasMany(MarCertificate)

MarCertificate.belongsTo(Student)
MarCertificate.belongsTo(MarCategory)

MoocCertificate.belongsTo(Student)

Department.hasMany(Student)
Department.hasMany(Faculty)

Student.belongsTo(Department)
Student.belongsTo(Faculty)
Student.hasMany(MoocCertificate)
Student.hasMany(MarCertificate)

Faculty.belongsTo(Department)
Faculty.hasMany(Student)

module.exports = {
  Student,
  Department,
  Faculty,
  Admin,
  MarCategory,
  MarCertificate,
  MoocCertificate
}
