const { Model, DataTypes } = require('sequelize');
const sequelize = require('../database')

class Faculty extends Model { }
Faculty.init({
    empId: {
        type: DataTypes.STRING,
        primaryKey: true
    },
    name: {
        type: DataTypes.STRING,
        allowNull: false,
    },
    email: {
        type: DataTypes.STRING,
        allowNull: false,
        unique: true,
    },
    displayImage: {
      type: DataTypes.STRING,
      allowNull: true,
    },
    phone: {
      type: DataTypes.STRING,
      allowNull: true,
      unique: true,
    },
    password_hash: {
      type: DataTypes.STRING,
      allowNull: false
    },

}, { sequelize, modelName: 'faculties', timestamps: true });

module.exports = Faculty
