const { Model, DataTypes } = require('sequelize');
const sequelize = require('../database')


class Admin extends Model { }
Admin.init({
    empId: {
        type: DataTypes.STRING,
        primaryKey: true
    },
    name: {
        type: DataTypes.STRING,
        allowNull: false,
    },
    email: {
        type: DataTypes.STRING,
        allowNull: false,
        unique: true,
    },
    displayImage: {
      type: DataTypes.STRING,
      allowNull: true,
    },
    phone: {
      type: DataTypes.STRING,
      allowNull: true,
      unique: true,
    },
    password_hash: {
      type: DataTypes.STRING,
      allowNull: false
    },

}, { sequelize, modelName: 'admins', timestamps: true });

module.exports = Admin
