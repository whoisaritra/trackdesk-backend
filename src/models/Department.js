const { Model, DataTypes } = require('sequelize');
const sequelize = require('../database')

class Department extends Model { }
Department.init({
    id: {
        type: DataTypes.STRING,
        primaryKey: true,
    },
    name: {
        type: DataTypes.STRING,
        allowNull: false,
    },
}, { sequelize, modelName: 'departments' });

module.exports = Department
