const { Model, DataTypes } = require('sequelize');
const sequelize = require('../database')

class Student extends Model { }
Student.init({
    rollNo: {
        type: DataTypes.STRING,
        primaryKey: true,
    },
    name: {
        type: DataTypes.STRING,
        allowNull: false,
    },
    email: {
        type: DataTypes.STRING,
        allowNull: false,
        unique: true,
    },
    displayImage: {
      type: DataTypes.STRING,
      allowNull: true,
    },
    phone: {
      type: DataTypes.STRING,
      allowNull: true,
      unique: true,
    },
    password_hash: {
      type: DataTypes.STRING,
      allowNull: false
    },
    admission_year: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
}, { sequelize, modelName: 'students', timestamps: true });

module.exports = Student


