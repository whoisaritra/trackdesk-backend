const fp = require('fastify-plugin')
const fastifyAuth = require("@fastify/auth")
var jwt = require('jsonwebtoken');
const {
  Student,
  Faculty,
  Admin
} = require('../models')

module.exports = fp((fastify, _, done) => {
  // fastify.addHook('onRequest', (request, reply, done) => {
  //   if (request.headers.authorization) {
  //     const token = request.headers.authorization.replace('Bearer ', '');
  //     const decodedToken = jwt.decode(token, process.env.JWTreqSECRET)
  //     if (!decodedToken) {
  //       reply.status(501).send({ message: "Not authorized" })
  //     } else {
  //       console.log(decodedToken)
  //       if (!decodedToken.id && !decodedToken.role) {
  //         reply.status(501).send({ message: "Not authorized" })
  //       }
  //     }

  //   } else {
  //     reply.status(501).send({ message: "Not authorized" })
  //   }
  //   done()
  // })

  fastify
    .decorate("verifyStudent", async (req, reply, done) => {
      const token = req.headers.authorization;
      const decodedToken = jwt.decode(token, process.env.JWTSECRET)
      if (decodedToken.role && ['student', 'admin', 'faculty'].includes(decodedToken.role)) {
        const student = await Student.findOne({ where: { rollNo: decodedToken.id } })
        if (!student) {
          reply.status(501).send({ error: "Student does not exist" })
          return;
        }
      }
      done();
    })
    .decorate("verifyFaculty", async (req, reply, done) => {
      const token = req.headers.authorization;
      const decodedToken = jwt.decode(token, process.env.JWTSECRET)
      if (decodedToken.role && ['faculty', 'admin'].includes(decodedToken.role)) {
        const faculty = await Faculty.findOne({ where: { empId: decodedToken.id } })
        if (!faculty) {
          reply.status(501).send({ error: "Not Authorized" })
          return;
        }
      }
      done();
    })
    .decorate("verifyAdmin", async (req, reply, done) => {
      const token = req.headers.authorization;
      const decodedToken = jwt.decode(token, process.env.JWTSECRET)
      if (decodedToken.role && ['admin'].includes(decodedToken.role)) {
        const admin = await Admin.findOne({ where: { empId: decodedToken.id } })
        if (!admin) {
          reply.status(501).send({ error: "Not Authorized" })
          return;
        }
      }

      done();
    }).register(fastifyAuth);

  done()
});

