const bcrypt = require('bcrypt')
const { Faculty } = require('../../models')
const schema = require('./schema')
const jwt = require('jsonwebtoken')

module.exports = (app, _, done) => {
  // Create Account
  app.post('/', {
    schema: schema.createFaculty,
    // preHandler: app.auth([app.verifyAdmin])
  }, async (req, reply) => {
    try {
      const faculty = req.body;
      faculty['password_hash'] = bcrypt.hashSync(req.body.password, 10);
      delete faculty['password'];

      await Faculty.create({
        ...faculty
      })
      return { message: 'Faculty Created' }
    } catch (err) {
      console.log(err)
      reply.status(500).send({ err: err })
    }
  })

  // Get Account
  app.get('/', {
    schema: schema.getFacultys,
    // preHandler: app.auth([app.verifyStudent])
  }, async (req, reply) => {
    try {
      let pageOptions = {};
      if (req.query.page) {
        pageOptions['offset'] = (req.query.page - 1) * 20;
        pageOptions['limit'] = 20;
      }
      const faculties = await Faculty.findAndCountAll({
        where: {
          ...req.query,
        },
        ...pageOptions
      });
      return faculties
    } catch (err) {
      console.log(err)
      reply.status(500).send({ err: err })
    }
  })

  // Get One Account
  app.get('/:id', { schema: schema.getOneFaculty }, async (req, reply) => {
    try {
      const faculties = await Faculty.findByPk(req.params.id);
      return faculties
    } catch (err) {
      console.log(err)
      reply.status(500).send({ err: err })
    }
  })

  // Update Account
  app.patch('/updatePassword', { schema: schema.updatePassword, preHandler: app.auth([app.verifyFaculty]) }, async (req, reply) => {
    const jwtParsed = jwt.decode(req.headers.authorization, 'MeowMeow')
    try {
      const { prevPassword, newPassword } = req.body;
      const admin = await Faculty.findByPk(jwtParsed.empId);
      if (admin && bcrypt.compareSync(prevPassword, admin.password_hash)) {
        admin.update({
          password_hash: bcrypt.hashSync(newPassword, 10)
        })
      }

      reply.send({ message: 'Password has been updated!' });
    } catch (err) {
      console.log(err)
      reply.status(500).send({ err: err })
    }
  })

  // Update Account
  app.patch('/:id', { schema: schema.updateFaculty}, async (req, reply) => {
    try {
      const newFacultyData = req.body;
      delete newFacultyData['password_hash']
      console.log(req.params.id);
      const faculty = await Faculty.findOne({ where: { empId: req.params.id}});
      await faculty.update(newFacultyData)

      return { message: 'Faculty Updated' }
    } catch (err) {
      console.log(err)
      reply.status(500).send({ err: err })
    }
  })

  // Delete One Account
  app.delete('/:id', { schema: schema.deleteFaculty, preHandler: app.auth([app.verifyFaculty]) }, async (req, reply) => {
    try {
      await Faculty.destroy({
        where: {
          empId: req.params.id
        }
      })
    } catch (err) {
      console.log(err)
      reply.status(500).send({ err: err })
    }
  })

  done()
}
