
const facultySchema = {
  type: 'object',
  properties: {
    name: { type: 'string' },
    empId: { type: 'string' },
    email: { type: 'string' },
    displayImage: { type: 'string' },
    phone: { type: 'string' },
    departmentId: { type: 'string' },
  }
}

const successResponse = {
  type: 'object',
  properties: {
    message: { type: 'string' }
  }
};

const errorResponse = {
  type: 'object',
  properties: {
    error: { type: 'string' }
  }
}

const createFaculty = {
  tags: ['Faculty'],
  body: {
    type: 'object',
    properties: {
      name: { type: 'string' },
      empId: { type: 'string' },
      email: { type: 'string' },
      displayImage: { type: 'string' },
      phone: { type: 'string' },
      password: { type: 'string' }
    }
  },
  response: {
    200: successResponse,
    default: errorResponse
  }
}

const getOneFaculty = {
  tags: ['Faculty'],
  params: {
    type: 'object',
    properties: {
      id: { type: 'string' }
    }
  },
  response: {
    default: errorResponse,
    200: facultySchema
  }
}


const getFacultys = {
  tags: ['Faculty'],
  querystring: {
    type: 'object',
    properties: {
      page: {
        type: 'number',
        minimum: 1,
      },
      departmentId: {
        type: 'string'
      }
    }
  },
  response: {
    default: errorResponse,
    200: {
      type: 'object',
      properties: {
        rows: {
          type: 'array',
          items: facultySchema
        },
        count: { type: 'number' }
      },
    }
  }
}

const updatePassword = {
  tags: ['Faculty'],
  body: {
    prevPassword: { type: 'string' },
    newPassword: { type: 'string' }
  },
  response: {
    200: successResponse,
    default: errorResponse,
  }
}

const updateFaculty = {
  tags: ['Faculty'],
  body: {
    name: { type: 'string' },
    email: { type: 'string' },
    contact: { type: 'string' },
    displayImage: { type: 'string' },
  },
  response: {
    200: successResponse,
    default: errorResponse,
  }
}

const deleteFaculty = {
  tags: ['Faculty'],
  params: {
    id: { type: 'string' }
  },
  response: {
    200: successResponse,
    default: errorResponse,
  }
}

module.exports = {
  createFaculty,
  getOneFaculty,
  getFacultys,
  updatePassword,
  updateFaculty,
  deleteFaculty,
}
