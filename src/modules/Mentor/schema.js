const facultySchema = {
  type: 'object',
  properties: {
    name: { type: 'string' },
    empId: { type: 'string' },
    email: { type: 'string' },
    displayImage: { type: 'string' },
    departmentId: { type: 'string' },
    phone: { type: 'string' },
  }
}

const studentSchema = {
  type: 'object',
  properties: {
    name: { type: 'string' },
    rollNo: { type: 'string' },
    email: { type: 'string' },
    displayImage: { type: 'string' },
    phone: { type: 'string' },
    admission_year: { type: 'integer' },
    departmentId: { type: 'string' },
  }
}

const successResponse = {
  type: 'object',
  properties: {
    message: { type: 'string' }
  }
};

const errorResponse = {
  type: 'object',
  properties: {
    error: { type: 'string' }
  }
}


const assignStudent = {
  tags: ['Mentor'],
  body: {
    type: 'object',
    properties: {
      mentorEmpId: { type: 'string' },
      students: { type: 'array', items: { type: 'string' } },
    }
  },
  response: {
    200: successResponse,
    default: errorResponse
  }
}

const unassignStudent = {
  tags: ['Mentor'],
  body: {
    type: 'object',
    properties: {
      studentRollNo: { type: 'string' }
    }
  },
  response: {
    200: successResponse,
    default: errorResponse
  }
}

const studentMentor = {
  tags: ['Mentor'],
  params: {
    type: 'object',
    properties: {
      studentRollNo: { type: 'string' }
    }
  },
  response: {
    200: facultySchema,
    default: errorResponse
  }
}

const getMentees = {
  tags: ['Mentor'],
  params: {
    type: 'object',
    properties: {
      mentorEmpId: { type: 'string' }
    }
  },
  response: {
    200: {
      type: 'object',
      properties: {
        rows: {
          type: 'array',
          items: studentSchema
        },
        count: { type: 'number' }
      },
    },
    default: errorResponse
  }
}

const getMentors = {
  tags: ['Mentor'],
  querystring: {
    type: 'object',
    properties: {
      departmentId: { type: 'string' },
    }
  },
  response: {
    200: {
      type: 'object',
      properties: {
        rows: {
          type: 'array',
          items: {
            type: 'object',
            properties: {
              name: { type: 'string' },
              email: { type: 'string' },
              phone: { type: 'string' },
              empId: { type: 'string' },
              mentees: { type: 'number' },
            }
          }
        },
        count: { type: 'number' }
      },
    },
    default: errorResponse
  }
}
module.exports = {
  assignStudent,
  unassignStudent,
  getMentees,
  studentMentor,
  getMentors,
}
