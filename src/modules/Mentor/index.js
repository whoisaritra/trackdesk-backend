const schema = require('./schema');
const sequelize = require('sequelize');
const { Student, Faculty } = require('../../models')

module.exports = (app, _, next) => {
  // Assign Mentee to a Mentor
  app.patch("/assign", { schema: schema.assignStudent }, async (req, reply) => {
    try {
      const { mentorEmpId, students } = req.body;
      const mentor = await Faculty.findByPk(mentorEmpId);
      if (mentor == null) {
        throw new Error("Mentor not found");
      }

      console.log(students);

      for(let i = 0; i < students.length; i++){
        const res = await Student.update({ facultyEmpId: mentor.empId }, { where: { rollNo: students[i] }});
        console.log(res);
      }

      reply.send({ message: "Mentor Assigned." })
    } catch (err) {
      console.log(err)
      reply.status(401).send({ err: err.message })
    }
  })

  // Remove Mentee from a Mentor
  app.delete("/assign", { schema: schema.unassignStudent }, async (req, reply) => {
    try {
      const { studentRollNo } = req.body;
      const student = await Faculty.findByPk(studentRollNo);
      if (student == null) {
        throw new Error("Student not found");
      }

      await student.update({
        facultyEmpId: null
      })
    } catch (err) {
      console.log(err)
      reply.status(401).send({ err: err.message })
    }
  })

  // Get Mentor of a Mentee
  app.get("/:studentRollNo/mentor", { schema: schema.studentMentor }, async (req, reply) => {
    try {
      const student = await Student.findByPk(req.params.studentRollNo);
      if (student.facultyEmpId == null) throw new Error("Mentor not assigned.")
      const mentor = await Faculty.findByPk(student.facultyEmpId);

      console.log(student)

      return mentor;
    } catch (err) {
      console.log(err)
      reply.status(401).send({ err: err.message })
    }
  })

  // Get All the Mentees of a Mentor
  app.get("/:mentorEmpId/mentees", { schema: schema.getMentees }, async (req, reply) => {
    try {
      const mentor = await Faculty.findByPk(req.params.mentorEmpId);
      if (mentor == null) throw new Error('Mentor does not exist')

      let pageOptions = {};
      if (req.query.page) {
        pageOptions['offset'] = (req.query.page - 1) * 20;
        pageOptions['limit'] = 20;
      }
      pageOptions['facultyEmpId'] = mentor.empId;
      const students = Student.findAndCountAll({
        where: {
          facultyEmpId: mentor.empId,
        },
        ...pageOptions
      })

      return students;
    } catch (err) {
      console.log(err)
      reply.status(401).send({ err: err.message })
    }
  })

  // Get All the Mentees of a Mentor
  app.get("/mentors", { schema: schema.getMentors }, async (req, reply) => {
    try {
      let pageOptions = {};
      if(req.query.page){
        pageOptions['offset'] = (req.query.page - 1) * 20;
        pageOptions['limit'] = 20;
      }
      const faculties= await Faculty.findAndCountAll({
        where: {
          ...req.query,
        },
        attributes: ['empId', 'name', 'departmentId', 'phone', 'email'].concat([
            [sequelize.literal('(SELECT COUNT("students"."rollNo") FROM "students" WHERE "students"."facultyEmpId" = "faculties"."empId")'), 'mentees']
        ]),
        ...pageOptions
      });

      return faculties
    } catch (err) {
      console.log(err)
      reply.status(401).send({ err: err.message })
    }
  })
  next()
}
