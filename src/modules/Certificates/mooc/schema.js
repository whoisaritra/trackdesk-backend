const certificateSchema = {
  type: 'object',
  properties: {
    id: { type: 'integer' },
    image: { type: 'string' },
    date: { type: 'string' },
    year: { type: 'integer' },
    courseName: { type: 'string' },
    duration: { type: 'integer'}
  }
}

const errorResponse = {
  type: 'object',
  properties: {
    error: { type: 'string' }
  }
}

const successResponse = {
  type: 'object',
  properties: {
    message: { type: 'string' }
  }
}

const getCertificates = {
  tags: ['MoocCertificate'],
  querystring: {
    type: 'object',
    properties: {
      year: { type: 'integer'}
    }
  },
  params: {
    type: 'object',
    properties: {
      studentRollNo: { type: 'string' }
    }
  },
  response: {
    200: {
      type: 'array',
      items: certificateSchema
    },
    default: errorResponse
  }
}

const getCertificate = {
  tags: ['MoocCertificate'],
  params: {
    type: 'object',
    properties: {
      studentRollNo: { type: 'string' }
    }
  },
  response: {
    200: certificateSchema,
    default: errorResponse
  }
}

const createCertificate = {
  tags: ['MoocCertificate'],
  body: certificateSchema,
  response: {
    200: certificateSchema,
    default: errorResponse,
  },
  security: [
    {
      "apiKey": []
    }
  ]
}

const updateCertificate = {
  tags: ['MoocCertificate'],
  body: certificateSchema,
  response: {
    200: successResponse,
    default: errorResponse
  }
}

const deleteCertificate = {
  tags: ['MoocCertificate'],
  params: {
    type: 'object',
    properties: {
      id: { type: 'string' }
    }
  },
  response: {
    200: certificateSchema,
    default: errorResponse
  }
}

module.exports = {
  getCertificate,
  getCertificates,
  createCertificate,
  updateCertificate,
  deleteCertificate,
}
