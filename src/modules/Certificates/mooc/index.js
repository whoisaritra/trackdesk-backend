const { MoocCertificate, Student } = require('../../../models')
const schema = require('./schema')
const jwt = require('jsonwebtoken')

module.exports = (app, _, done) => {
  app.get('/:id', { schema: schema.getCertificate } ,async (req, reply) => {
    try {
      const certificate = await MoocCertificate.findByPk(req.params.id)

      return certificate
    } catch (err) {
      console.log(err)
      reply.status(401).send({ error: "Some error occured" })
    }
  })

  app.get('/student/:studentRollNo',{ schema: schema.getCertificates }, async (req, reply) => {
    try {
      const student = await Student.findByPk(req.params.studentRollNo)
      if(student == null) throw new Error("Student not found");

      const whereQuery = {};
      whereQuery['studentRollNo'] = student.rollNo;
      if(req.query.year) whereQuery['year'] = req.query.year;

      const certificates = await MoocCertificate.findAll({
        where: whereQuery,
      })

      return certificates
    } catch (err) {
      console.log(err)
      reply.status(401).send({ error: err.message })
    }
  })

  app.post('/',{ schema: schema.createCertificate }, async (req, reply) => {
    try {
      const jwtParsed = jwt.decode(req.headers.authorization, 'MeowMeow');
      const student = await Student.findOne({ where: { rollNo: jwtParsed.rollNo } })
      if (!student) {
        return reply.status(404).send({ error: "Student not found" })
      }

      const certificate = await MoocCertificate.create({
        ...req.body,
        studentRollNo: student.rollNo
      })

      return certificate;
    } catch (err) {
      console.log(err)
      reply.status(401).send({ error: "Some error occured" })
    }
  })

  app.patch('/:id',{ schema: schema.updateCertificate }, async (req, reply) => {
    try {
      // Check Student Exists
      const certificate = await MoocCertificate.findOne({ where: { id: req.params.id } })
      if (!certificate) {
        return reply.status(404).send({ error: "Certifcate not found" })
      }

      await certificate.update({
        ...req.body,
      })

      return { message: "Certificate Updated" }
    } catch (err) {
      console.log(err)
      reply.status(401).send({ error: "Some error occured" })
    }
  })

  app.delete('/:id', { schema: schema.deleteCertificate } ,async (req, reply) => {
    try {
      await MoocCertificate.destroy({
        where: {
          id: parseInt(req.params.id)
        }
      })

      return { message: "Certificate Deleted" }
    } catch (err) {
      console.log(err)
      reply.status(401).send({ error: "Some error occured" })
    }
  })

  done()
}
