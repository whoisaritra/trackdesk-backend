const { MarCategory } = require('../../../models')
const schema = require('./schema')

module.exports = (app, _, done) => {
  app.get('/', { schema: schema.getCategories } ,async (_, reply) => {
    try {
      const categories = await MarCategory.findAll()
      return categories;
    } catch (err) {
      console.log(err)
      reply.status(500).send({ err: err })
    }
  })

  app.get('/:id', { schema: schema.getCategory}, async (req, reply) => {
    try {
      const category = await MarCategory.findByPk(req.params.id)
      return category;
    } catch (err) {
      console.log(err)
      reply.status(500).send({ err: err })
    }
  })

  app.post('/',{ schema: schema.createCategory} ,async (req, reply) => {
    try {
      await MarCategory.create({
        category: req.body.category,
        points: req.body.points,
        max_points: req.body.max_points
      })
      return { message: 'Category Created' };
    } catch (err) {
      console.log(err)
      reply.status(500).send({ err: err })
    }
  })

  app.patch('/:id', { schema: schema.updateCategory } ,async (req, reply) => {
    try {
      const category = await MarCategory.findByPk(req.params.id);
      await category.update({
        ...req.body
      })

      return { message: 'Category Updated' };
    } catch (err) {
      console.log(err)
      reply.status(500).send({ err: err })
    }
  })

  app.delete('/:id', { schema: schema.deleteCategory }, async (req, reply) => {
    try {
      await MarCategory.destroy({ where: { id: req.params.id } });
      return { message: 'Category Deleted' };
    } catch (err) {
      console.log(err)
      reply.status(500).send({ err: err })
    }
  })
  done()
}
