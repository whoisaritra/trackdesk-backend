const categorySchema = {
  type: 'object',
  properties: {
    id: { type: 'number' },
    category: { type: 'string' },
    points: { type: 'integer' },
    max_points: { type: 'integer' }
  }
}

const errorResponse = {
  type: 'object',
  properties: {
    error: { type: 'string' }
  }
}

const successResponse = {
  type: 'object',
  properties: {
    message: { type: 'string' }
  }
}

const getCategories = {
  tags: ['MarCategory'],
  response: {
    200: {
      type: 'array',
      items: categorySchema
    },
    default: errorResponse
  }
}

const getCategory = {
  tags: ['MarCategory'],
  params: {
    type: 'object',
    properties: {
      id: { type: 'string' }
    }
  },
  response: {
    200: categorySchema,
    default: errorResponse
  }
}

const createCategory = {
  tags: ['MarCategory'],
  body: categorySchema,
  response: {
    200: successResponse,
    default: errorResponse
  }
}

const updateCategory = {
  tags: ['MarCategory'],
  body: categorySchema,
  response: {
    200: successResponse,
    default: errorResponse
  }
}

const deleteCategory = {
  tags: ['MarCategory'],
  params: {
    type: 'object',
    properties: {
      id: { type: 'string' }
    }
  },
  response: {
    200: categorySchema,
    default: errorResponse
  }
}

module.exports = {
  getCategory,
  getCategories,
  createCategory,
  updateCategory,
  deleteCategory,
}
