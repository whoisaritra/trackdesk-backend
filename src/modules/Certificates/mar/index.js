const { MarCertificate, Student, MarCategory } = require('../../../models')
const schema = require('./schema')
const jwt = require('jsonwebtoken');

module.exports = (app, _, done) => {
  app.get('/:id', { schema: schema.getCertificate } ,async (req, reply) => {
    try {
      const certificate = await MarCertificate.findOne({ where: { id: req.params.id }, include: [ MarCategory ]})

      return certificate
    } catch (err) {
      console.log(err)
      reply.status(401).send({ error: "Some error occured" })
    }
  })

  app.get('/student/:studentRollNo', { schema: schema.getCertificates },async (req, reply) => {
    try {
      const certificates = await MarCertificate.findAll({
        where: {
          studentRollNo: req.params.studentRollNo
        },
        include: [MarCategory]
      })

      let certs = {};
      certificates.forEach((cert) => {
       certs[cert.marcategoryId]
      })

      return certificates
    } catch (err) {
      console.log(err)
      reply.status(401).send({ error: "Some error occured" })
    }
  })

  app.post('/',{ schema: schema.createCertificate }, async (req, reply) => {
    try {
      const jwtParsed = jwt.decode(req.headers.authorization, process.env.JWTSECRET);
      // Check Student Exists
      const student = await Student.findOne({ where: { rollNo: jwtParsed.rollNo == null ? req.body.studentRollNo : jwtParsed.rollNo } })
      if (!student) {
        return reply.status(404).send({ error: "Student not found" })
      }

      const category = await MarCategory.findOne({
        where: {
          id: req.body.marcategoryId,
        }
      });
      const certificates = await MarCertificate.findAll({
        where: {
          studentRollNo: jwtParsed.rollNo == null ? req.body.studentRollNo : jwtParsed.rollNo,
          marcategoryId: req.body.marcategoryId
        }
      }) || [];

      let totalPoints = category.points * certificates.length;
      console.log('asdasd', totalPoints, category['points'], category['max_points']);

      if (totalPoints + parseInt(category['points']) > parseInt(category['max_points'])) {
        return reply.status(401).send({ error: "Maximum Points Reached" })
      }

      const certificate = await MarCertificate.create({
        ...req.body,
        studentRollNo: student.rollNo
      })

      return certificate;
    } catch (err) {
      console.log(err)
      reply.status(401).send({ error: "Some error occured" })
    }
  })

  app.patch('/:id', { schema: schema.updateCertificate } , async (req, reply) => {
    try {
      // Check Student Exists
      const certificate = await MarCertificate.findOne({ where: { id: req.params.id } })
      if (!certificate) {
        return reply.status(404).send({ error: "Certificate not found" })
      }

      await certificate.update({
        ...req.body,
      })

      return { message: "Certificate Updated" }
    } catch (err) {
      console.log(err)
      reply.status(401).send({ error: "Some error occured" })
    }
  })

  app.delete('/:id', { schema: schema.deleteCertificate } ,async (req, reply) => {
    try {
      await MarCertificate.destroy({
        where: {
          id: parseInt(req.params.id)
        }
      })

      return { message: "Certificate Deleted" }
    } catch (err) {
      console.log(err)
      reply.status(401).send({ error: "Some error occured" })
    }
  })

  done()
}
