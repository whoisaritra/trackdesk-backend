const certificateSchema = {
  type: 'object',
  properties: {
    id: { type: 'integer' },
    image: { type: 'string' },
    date: { type: 'string' }, year: { type: 'integer' },
    marcategoryId: { type: 'integer'},
    eventName: { type: 'string' },
    points: { type: 'integer' },
    studentRollNo: { type: 'string' },
  }
}

const errorResponse = {
  type: 'object',
  properties: {
    error: { type: 'string' }
  }
}

const successResponse = {
  type: 'object',
  properties: {
    message: { type: 'string' }
  }
}

const getCertificates = {
  tags: ['MarCertificate'],
  params: {
    type: 'object',
    properties: {
      studentRollNo: { type: 'string' }
    }
  },
  response: {
    200: {
      type: 'array',
      items: certificateSchema
    },
    default: errorResponse
  }
}

const getCertificate = {
  tags: ['MarCertificate'],
  params: {
    type: 'object',
    properties: {
      id: { type: 'number' }
    }
  },
  response: {
    200: certificateSchema,
    default: errorResponse
  }
}

const createCertificate = {
  tags: ['MarCertificate'],
  body: {
    type: 'object',
    properties: {
      image: { type: 'string' },
      date: { type: 'string' },
      year: { type: 'integer' },
      eventName: { type: 'string' },
      marcategoryId: { type: 'number' },
    }
  },
  response: {
    200: certificateSchema,
    default: errorResponse
  },
  security: [
    {
      "apiKey": []
    }
  ]
}

const updateCertificate = {
  tags: ['MarCertificate'],
  body: certificateSchema,
  response: {
    200: successResponse,
    default: errorResponse
  }
}

const deleteCertificate = {
  tags: ['MarCertificate'],
  params: {
    type: 'object',
    properties: {
      id: { type: 'string' }
    }
  },
  response: {
    200: certificateSchema,
    default: errorResponse
  }
}

module.exports = {
  getCertificate,
  getCertificates,
  createCertificate,
  updateCertificate,
  deleteCertificate,
}
