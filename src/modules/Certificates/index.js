module.exports = (app, _, done) => {
  app.register(require('./mar_category'), { prefix: 'mar/categories' })
  app.register(require('./mar'), { prefix: 'mar' })
  app.register(require('./mooc'), { prefix: 'moocs' })

  done()
}
