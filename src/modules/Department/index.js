const { Department } = require('../../models')
const schema = require("./schema")


module.exports = (app, _, next) => {
  app.get("/", { schema: schema.getDepartments } ,async (req, reply) => {
    try {
      const departments = await Department.findAll()
      return departments
    } catch (err) {
      console.log(err);
      reply.status(401).send({ error: err.message })
    }
  })

  app.get("/:id", { schema: schema.getDepartment } ,async (req, reply) => {
    try {
      const department = await Department.findByPk(req.params.id)
      return department
    } catch (err) {
      console.log(err);
      reply.status(401).send({ error: err.message })
    }
  })

  app.post("/", { schema: schema.createDepartment } ,async (req, reply) => {
    try {
      await Department.create({ ...req.body })
      reply.send({ message: "Department Created! " })
    } catch (err) {
      console.log(err);
      reply.status(401).send({ error: err.message })
    }
  })

  app.patch("/:id", { schema: schema.updateDepartment } ,async (req, reply) => {
    try {
      const dept = await Department.findByPk(req.params.id);
      await dept.update({ ...req.body })
      reply.send({ message: "Department Updated! " })
    } catch (err) {
      console.log(err);
      reply.status(401).send({ error: err.message })
    }
  })


  app.delete("/:id", { schema: schema.deleteDepartment } ,async (req, reply) => {
    try {
      await Department.destroy({ where: { id: req.params.id } });
      reply.send({ message: "Department Updated! " })
    } catch (err) {
      console.log(err);
      reply.status(401).send({ error: err.message })
    }
  })
  next();
}
