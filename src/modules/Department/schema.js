const departmentSchema = {
  type: 'object',
  properties: {
    id: { type: 'string' },
    name: { type: 'string' }
  }
}

const errorResponse = {
  type: 'object',
  properties: {
    error: { type: 'string' }
  }
}

const successResponse = {
  type: 'object',
  properties: {
    message: { type: 'string' }
  }
}

const getDepartments = {
  tags: ['Department'],
  response: {
    200: {
      type: 'array',
      items: departmentSchema
    },
    default: errorResponse
  }
}

const getDepartment = {
  tags: ['Department'],
  params: {
    type: 'object',
    properties: {
      id: { type: 'string' }
    }
  },
  response: {
    200: departmentSchema,
    default: errorResponse
  }
}

const createDepartment = {
  tags: ['Department'],
  body: departmentSchema,
  response: {
    200: successResponse,
    default: errorResponse
  }
}

const updateDepartment = {
  tags: ['Department'],
  body: departmentSchema,
  response: {
    200: successResponse,
    default: errorResponse
  }
}

const deleteDepartment = {
  tags: ['Department'],
  params: {
    type: 'object',
    properties: {
      id: { type: 'string' }
    }
  },
  response: {
    200: departmentSchema,
    default: errorResponse
  }
}

module.exports = {
  getDepartment,
  getDepartments,
  createDepartment,
  updateDepartment,
  deleteDepartment,
}
