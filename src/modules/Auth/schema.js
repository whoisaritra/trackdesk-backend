const loginSchema = {
  tags: [ 'Auth'],
  params: {
    type: 'object',
    properties: {
      user: {
        type: 'string',
        enum: ['admin', 'student', 'faculty']
      }
    }
  },
  body: {
    type: 'object',
    properties: {
      rollNo: { type: 'string'},
      empId: { type: 'string'},
      password: { type: 'string'},
    }
  },
  response: {
    200: {
      type: 'object',
      properties: {
        accessToken: { type: 'string' }
      }
    },
    default: {
      type: 'object',
      properties: {
        error: { type: 'string' }
      }
    }
  }
}


module.exports = {
  loginSchema
}

