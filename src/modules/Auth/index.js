const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const schema = require('./schema')

const {
  Student,
  Admin,
  Faculty
} = require('../../models')


module.exports = (app, _, done) => {
  app.post('/login/:user',{ schema: schema.loginSchema } ,async (req, reply) => {
    const reqBody = req.body;
    console.log(req.body.rollNo)
    let user;
    let payload = {};
    try {
      switch (req.params.user) {
        case 'admin':
          user = await Admin.findOne({ where: { empId: req.body.empId } })
          payload['empId'] = user && user.empId;
          payload['role'] = user && 'admin';
          break;
        case 'student':
          user = await Student.findOne({ where: { rollNo: req.body.rollNo}  })
          payload['rollNo'] = user && user.rollNo;
          payload['role'] = user && 'student';
          payload['department'] = user && user.departmentId
          break;
        case 'faculty':
          user = await Faculty.findOne({ where: { empId: req.body.empId } })
          payload['empId'] = user && user.empId;
          payload['role'] = user && 'faculty';
          payload['department'] = user && user.departmentId
          break;
        default:
          return reply.status(404).send({ error: "Not valid User" })
      }
      if (!user) {
        return reply.status(404).send({ error: "User not found" })
      }

      if (bcrypt.compareSync(reqBody.password, user.password_hash)) {
        const token = jwt.sign(payload, process.env.JWTSECRET);
        return reply.send({ accessToken: token })
      } else {
        return reply.status(404).send({ error: "Check Credentials" })
      }
    } catch (err) {
      console.log(err)
      return reply.status(404).send({ error: err.message })
    }
  })

  done()
}

