const studentSchema = {
  type: 'object',
  properties: {
    name: { type: 'string' },
    rollNo: { type: 'string' },
    email: { type: 'string' },
    displayImage: { type: 'string' },
    phone: { type: 'string' },
    admission_year: { type: 'integer' },
    departmentId: { type: 'string' },
  }
}

const successResponse = {
  type: 'object',
  properties: {
    message: { type: 'string' }
  }
};

const errorResponse = {
  type: 'object',
  properties: {
    error: { type: 'string' }
  }
}

const createStudent = {
  tags: ['Student'],
  body: {
    type: 'object',
    properties: {
      name: { type: 'string' },
      rollNo: { type: 'string' },
      email: { type: 'string' },
      displayImage: { type: 'string' },
      phone: { type: 'string' },
      admission_year: { type: 'integer' },
      password: { type: 'string' },
      departmentId: { type: 'string' }
    }
  },
  response: {
    200: successResponse,
    default: errorResponse
  }
}

const getOneStudent = {
  tags: ['Student'],
  params: {
    type: 'object',
    properties: {
      id: { type: 'string' }
    }
  },
  response: {
    default: errorResponse,
    200: studentSchema
  }
}


const getStudents = {
  tags: ['Student'],
  querystring: {
    type: 'object',
    properties: {
      page: {
        type: 'number',
        minimum: 1,
      },
      departmentId: {
        type: 'string'
      }
    }
  },
  response: {
    default: errorResponse,
    200: {
      type: 'object',
      properties: {
        rows: {
          type: 'array',
          items: studentSchema
        },
        count: { type: 'number' }
      },
    }
  }
}

const updatePassword = {
  tags: ['Student'],
  body: {
    prevPassword: { type: 'string' },
    newPassword: { type: 'string' }
  },
  response: {
    200: successResponse,
    default: errorResponse,
  },
  security: [
    {
      "apiKey": []
    }
  ]
}

const updateStudent = {
  tags: ['Student'],
  body: {
    name: { type: 'string' },
    email: { type: 'string' },
    displayImage: { type: 'string' },
  },
  response: {
    200: successResponse,
    default: errorResponse,
  },
}

const deleteStudent = {
  tags: ['Student'],
  params: {
    id: { type: 'string' }
  },
  response: {
    200: successResponse,
    default: errorResponse,
  },
  security: [
    {
      "apiKey": []
    }
  ]
}

module.exports = {
  createStudent,
  getOneStudent,
  getStudents,
  updatePassword,
  updateStudent,
  deleteStudent,
}
