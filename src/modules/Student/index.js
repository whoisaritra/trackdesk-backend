const bcrypt = require('bcrypt')
const { Student } = require('../../models')
const schema = require('./schema')
const jwt = require('jsonwebtoken')

module.exports = (app, _, done) => {
  // Create Account
  app.post('/', {
    schema: schema.createStudent,
    // preHandler: app.auth([app.verifyAdmin])
  }, async (req, reply) => {
    try {
      const student = req.body;
      student['password_hash'] = bcrypt.hashSync(req.body.password, 10);
      delete student['password'];

      await Student.create({
        ...student
      })

      return { message: 'Student Created' }
    } catch (err) {
      console.log(err)
      reply.status(500).send({ err: err })
    }
  })

  // Get Account
  app.get('/', { schema: schema.getStudents }, async (req, reply) => {
    try {
      let pageOptions = {};
      if(req.query.page){
        pageOptions['offset'] = (req.query.page - 1) * 20;
        pageOptions['limit'] = 20;
      }
      const students = await Student.findAndCountAll({
        where: {
          ...req.query,
        },
        ...pageOptions
      });
      return students
    } catch (err) {
      console.log(err)
      reply.status(500).send({ err: err })
    }
  })

  // Get One Account
  app.get('/:id', { schema: schema.getOneStudent }, async (req, reply) => {
    try {
      const students = await Student.findByPk(req.params.id);
      return students
    } catch (err) {
      console.log(err)
      reply.status(500).send({ err: err })
    }
  })

  // Update Account
  app.patch('/updatePassword', { schema: schema.updatePassword, preHandler: app.auth([app.verifyStudent]) }, async (req, reply) => {
    try {
      const jwtParsed = jwt.decode(req.headers.authorization, 'MeowMeow')
      const { prevPassword, newPassword } = req.body;
      const admin = await Student.findByPk(jwtParsed.rollNo);
      console.log(prevPassword, admin.password_hash)
      if (admin != null && bcrypt.compareSync(prevPassword, admin.password_hash)) {
        await admin.update({
          password_hash: bcrypt.hashSync(newPassword, 10)
        })
      } else {
        return reply.status(400).send({ error: "Check credentials"})
      }

      reply.send({ message: 'Password has been updated!' });
    } catch (err) {
      console.log(err)
      reply.status(500).send({ err: err })
    }
  })

  // Update Account
  app.patch('/:id', { schema: schema.updateStudent }, async (req, reply) => {
    try {
      const newStudentData = req.body;
      delete newStudentData['password_hash']
      const student = await Student.findByPk(req.params.id);
      await student.update(newStudentData)

      return { message: 'Student Updated' }
    } catch (err) {
      console.log(err)
      reply.status(500).send({ err: err })
    }
  })

  // Delete One Account
  app.delete('/:id', { schema: schema.deleteStudent, preHandler: app.auth([app.verifyStudent]) }, async (req, reply) => {
    try {
      await Student.destroy({
        where: {
          rollNo: req.params.id
        }
      })
    } catch (err) {
      console.log(err)
      reply.status(500).send({ err: err })
    }
  })

  done()
}
