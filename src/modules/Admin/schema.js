
const successResponse = {
  type: 'object',
  properties: {
    message: { type: 'string' }
  }
};

const errorResponse = {
  type: 'object',
  properties: {
    error: { type: 'string' }
  }
}


const createAdmin = {
  tags: ['Admin'],
  body: {
    type: 'object',
    properties: {
      empId: { type: 'string' },
      name: { type: 'string' },
      email: { type: 'string' },
      password: { type: 'string' }
    }
  },
  response: {
    200: successResponse,
    default: errorResponse
  }
}

const getOneAdmin = {
  tags: ['Admin'],
  params: {
    type: 'object',
    properties: {
      id: { type: 'string' }
    }
  },
  response: {
    default: errorResponse,
    200: {
      type: 'object',
      properties: {
        empId: { type: 'string' },
        name: { type: 'string' },
        email: { type: 'string' },
      }
    }
  }
}


const getAdmins = {
  tags: ['Admin'],
  response: {
    default: errorResponse,
    200: {
      type: 'array',
      items: {
        type: 'object',
        properties: {
          empId: { type: 'string' },
          name: { type: 'string' },
          email: { type: 'string' },
        }
      }
    }
  }
}

const updatePassword = {
  tags: ['Admin'],
  body: {
    prevPassword: { type: 'string' },
    newPassword: { type: 'string' }
  },
  response: {
    200: successResponse,
    default: errorResponse,
  }
}

const updateAdmin = {
  tags: ['Admin'],
  body: {
    name: { type: 'string' },
    email: { type: 'string' },
  },
  response: {
    200: successResponse,
    default: errorResponse,
  }
}

const deleteAdmin = {
  tags: ['Admin'],
  params: {
    id: { type: 'string' }
  },
  response: {
    200: successResponse,
    default: errorResponse,
  }
}

module.exports = {
  createAdmin,
  getOneAdmin,
  getAdmins,
  updatePassword,
  updateAdmin,
  deleteAdmin,
}
