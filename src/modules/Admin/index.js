const bcrypt = require('bcrypt')
const { Admin } = require('../../models')
const schema = require('./schema')
const jwt = require('jsonwebtoken')

module.exports = (app, _, done) => {
  // Create Account
  app.post('/', {
    schema: schema.createAdmin,
    // preHandler: app.auth([app.verifyAdmin])
  }, async (req, reply) => {
    try {
      const admin = {
        empId: req.body.empId,
        name: req.body.name,
        email: req.body.email,
        password_hash: bcrypt.hashSync(req.body.password, 10)
      };

      await Admin.create(admin)

      return { message: 'Admin Created' }
    } catch (err) {
      console.log(err)
      reply.status(500).send({ err: err })
    }
  })

  // Get Account
  app.get('/', { schema: schema.getAdmins, preHandler: app.auth([app.verifyAdmin]) }, async (_, reply) => {
    try {
      const admins = await Admin.findAll();
      return admins
    } catch (err) {
      console.log(err)
      reply.status(500).send({ err: err })
    }
  })

  // Get One Account
  app.get('/:id', { schema: schema.getOneAdmin, preHandler: app.auth([app.verifyAdmin]) }, async (req, reply) => {
    try {
      const admins = await Admin.findByPk(req.params.id);
      return admins
    } catch (err) {
      console.log(err)
      reply.status(500).send({ err: err })
    }
  })

  // Update Account
  app.patch('/updatePassword', { schema: schema.updatePassword, preHandler: app.auth([app.verifyAdmin]) }, async (req, reply) => {
    try {
      const jwtParsed = jwt.decode(req.headers.authorization, 'MeowMeow')
      const { prevPassword, newPassword } = req.body;
      const admin = await Admin.findByPk(jwtParsed.empId);
      if(  admin && bcrypt.compareSync(prevPassword, admin.password_hash) ){
        admin.update({
          password_hash: bcrypt.hashSync(newPassword, 10)
        })
      }

      reply.send({ message: 'Password has been updated!' });
    } catch (err) {
      console.log(err)
      reply.status(500).send({ err: err })
    }
  })

  // Update Account
  app.patch('/:id', { schema: schema.updateAdmin, preHandler: app.auth([app.verifyAdmin]) }, async (req, reply) => {
    try {
      const newAdminData = req.body;
      delete newAdminData['password_hash']
      const admin = await Admin.findByPk(req.params.id);
      await admin.update(newAdminData)

      return { message: 'Admin Updated' }
    } catch (err) {
      console.log(err)
      reply.status(500).send({ err: err })
    }
  })

  // Delete One Account
  app.delete('/:id', { schema: schema.deleteAdmin, preHandler: app.auth([app.verifyAdmin]) }, async (req, reply) => {
    try {
      await Admin.destroy({
        where: {
          empId: req.params.id
        }
      })
    } catch (err) {
      console.log(err)
      reply.status(500).send({ err: err })
    }
  })

  done()
}
