const fastify = require('fastify')
require('dotenv').config();

// const database = require('./database');


const server = async () => {
  const app = fastify({
    logger: {
        prettyPrint: {
          translateTime: true,
          ignore: 'pid,hostname,reqId,responseTime,req,res',
          messageFormat: '{msg} {req.method} {req.url}'
        }
    }
  });

  app.register(require('@fastify/cors'), {});
  app.register(require('./plugins/swagger'));

  app.register(require('./plugins/auth'));
  app.register(require('./modules/Admin'), { prefix: '/api/v1/admin'})
  app.register(require('./modules/Auth'), { prefix: '/api/v1/auth'})
  app.register(require('./modules/Faculty'), { prefix: '/api/v1/faculties'})
  app.register(require('./modules/Student'), { prefix: '/api/v1/students'})
  app.register(require('./modules/Mentor'), { prefix: '/api/v1/mentors'})
  app.register(require('./modules/Certificates'), { prefix: '/api/v1/certificates'})
  app.register(require('./modules/Department'), { prefix: '/api/v1/departments'})

  try {
    // await database.sync();
    // app.listen(process.env.PORT, '0.0.0.0');
    app.listen(process.env.PORT | 8080);
  } catch (err) {
    console.error('Error: ', err);
  }
}


server();



