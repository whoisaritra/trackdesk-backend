const Sequelize = require('sequelize')

const connection = new Sequelize(process.env.DB_URL);
// const connection = new Sequelize(process.env.DB_NAME, process.env.DB_USERNAME, process.env.DB_PASSWORD, {
//     host: process.env.DB_HOST,
//     dialect: 'postgres'
// });

module.exports = connection

